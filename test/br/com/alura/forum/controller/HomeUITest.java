package br.com.alura.forum.controller;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

class HomeUITest {

	public static WebDriver browser;

	@BeforeAll
	public static void abrirBrowser() {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		browser = new ChromeDriver(options);
		
//		browser = new ChromeDriver();
	}

	@BeforeEach
	public void navegarTelaInicial() {
		browser.navigate().to("http://localhost:8080/alura-forum/topicos");
	}

	@Test
	public void deveriaExibirATelaInicial() {
		WebElement titulo = browser.findElement(By.xpath("/html/body/section/section[1]/div/h1/a"));
		Assertions.assertEquals("Fórum".toUpperCase(), titulo.getText().toUpperCase());
	}

	@Test
	public void deveriaExibirATelaDeCadastro() {
		browser.findElement(By.xpath("/html/body/header[2]/div/div/a[2]")).click();
		WebElement titulo = browser.findElement(By.xpath("/html/body/div[2]/section/section/p"));
		Assertions.assertEquals("Cadastre-se gratuitamente!".toUpperCase(), titulo.getText().toUpperCase());
	}

	@AfterAll
	public static void fecharBrowser() {
		browser.close();
	}
}
